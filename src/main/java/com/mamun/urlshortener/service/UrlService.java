package com.mamun.urlshortener.service;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.hash.Hashing;
import com.mamun.urlshortener.Entity.ClickData;
import com.mamun.urlshortener.Entity.EntryData;
import com.mamun.urlshortener.Entity.UrlData;
import com.mamun.urlshortener.Repository.ClickDataRepository;
import com.mamun.urlshortener.Repository.UrlDataRepository;

@Service
public class UrlService {
	@Autowired
	UrlDataRepository urlDataRepository;
	@Autowired
	ClickDataRepository clickDataRepository;
	
	String ip="";
	Logger log = LoggerFactory.getLogger(getClass());
	public void getDeviceList(HttpServletRequest httpServletRequest) {
        ip = httpServletRequest.getRemoteHost();
    }
	
	
	public UrlData generateUrl(EntryData entryData) {
		String shortLink =entryData.getShort_url_domain()+"-"+ generateShortLink(entryData.getLong_url());
		 
		 UrlData urlData = new UrlData();
		 urlData.setLongUrl(entryData.getLong_url());
		 urlData.setShortUrl(shortLink);
		 log.debug(urlData.getShortUrl()+" is generated for "+urlData.getLongUrl());
		 urlDataRepository.save(urlData);	
		 return urlData;
	}
	
	private String generateShortLink(String url) {
		
		String encodedUrl = "";
        LocalDateTime time = LocalDateTime.now();
        encodedUrl = Hashing.murmur3_32()
                .hashString(url.concat(time.toString()), StandardCharsets.UTF_8)
                .toString();
        return  encodedUrl;
	}


	public Optional<UrlData> getUrlById(int id) {
		log.debug("Showed data of id "+id);
		return urlDataRepository.findById(id);	
	}
	
	
	public UrlData findByShortUrl(String shortLink) {
		// TODO Auto-generated method stub
		clickDataSave(shortLink);
		UrlData urlToRet = urlDataRepository.findByshortUrl(shortLink);
		log.debug("Redirected to "+urlToRet.getLongUrl());
		return urlToRet;	
	}
	
	private void clickDataSave(String shortLink) {
		ClickData clickData = new ClickData();	
		clickData.setShortUrl(shortLink);	
		clickDataRepository.save(clickData);
	}
	
	
	public Iterable<ClickData> getAllClicks() {
		// TODO Auto-generated method stub
		log.debug("All ClickedData Showed");	
		return clickDataRepository.findAll();	
	}
	
	
	public long getCount() {
		// TODO Auto-generated method stub
		long n = urlDataRepository.count();
	
		log.debug("Total Number is"+n);
		return n;
	}


}
