package com.mamun.urlshortener.Entity;

import javax.persistence.*;


@Entity
public class UrlData {

	@Id
	@GeneratedValue
	int id;
	String longUrl;
	String shortUrl;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLongUrl() {
		return longUrl;
	}
	public void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}
	public String getShortUrl() {
		return shortUrl;
	}
	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}
	
	
}
