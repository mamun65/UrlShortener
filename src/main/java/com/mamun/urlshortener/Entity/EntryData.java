package com.mamun.urlshortener.Entity;

import java.util.Map;




public class EntryData {
	String long_url;
    String short_url_domain;
    Map<String, String> parameters;
	public String getLong_url() {
		return long_url;
	}
	public void setLong_url(String long_url) {
		this.long_url = long_url;
	}
	public String getShort_url_domain() {
		return short_url_domain;
	}
	public void setShort_url_domain(String short_url_domain) {
		this.short_url_domain = short_url_domain;
	}
	public Map<String, String> getParameters() {
		return parameters;
	}
	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}
	
	
	
	
}
