package com.mamun.urlshortener.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
public class ClickData {
	@Id
	@GeneratedValue
	int clickId;
	
    String shortUrl;
    String userIp;
    String userDevice;
    String userBrowser;
	public int getClickId() {
		return clickId;
	}
	public void setClickId(int clickId) {
		this.clickId = clickId;
	}
	public String getShortUrl() {
		return shortUrl;
	}
	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}
	
	public String getUserIp() {
		return userIp;
	}
	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}
	public String getUserDevice() {
		return userDevice;
	}
	public void setUserDevice(String userDevice) {
		this.userDevice = userDevice;
	}
	public String getUserBrowser() {
		return userBrowser;
	}
	public void setUserBrowser(String userBrowser) {
		this.userBrowser = userBrowser;
	}
    
}
