package com.mamun.urlshortener.Controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import com.google.common.hash.Hashing;
import com.mamun.urlshortener.Entity.ClickData;
import com.mamun.urlshortener.Entity.EntryData;
import com.mamun.urlshortener.Entity.UrlData;
import com.mamun.urlshortener.Repository.ClickDataRepository;
import com.mamun.urlshortener.Repository.UrlDataRepository;
import com.mamun.urlshortener.service.UrlService;


@RestController
public class ApiController {

	@Autowired
	private UrlService urlService;
	
	
	@PostMapping("/generate")
	public UrlData generateUrl(@RequestBody  EntryData entryData) {			 
		return urlService.generateUrl(entryData);		 
	}
	
	
	@GetMapping("/getRecord/{id}")
    public Optional<UrlData> getUrlById(@PathVariable int id) {		
		 return urlService.getUrlById(id);		
    }
		
	
	@GetMapping("redirect/{shortLink}")
    public void redirectToOriginalUrl(@PathVariable String shortLink, HttpServletResponse response) throws IOException {
		UrlData urlToRet = urlService.findByShortUrl(shortLink);
		response.sendRedirect(urlToRet.getLongUrl());        
    }
	
	@GetMapping("/getAllClicks")
    public @ResponseBody Iterable<ClickData> getAllData() {		
		return urlService.getAllClicks();		
    }
	
	
	@GetMapping("/count")
    public @ResponseBody long getCount() {		
		long n = urlService.getCount();		
		return n;
    }
}
