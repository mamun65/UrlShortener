package com.mamun.urlshortener.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mamun.urlshortener.Entity.ClickData;

public interface ClickDataRepository extends JpaRepository<ClickData, Integer> {

}
