package com.mamun.urlshortener.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mamun.urlshortener.Entity.UrlData;

@Repository
public interface UrlDataRepository extends JpaRepository<UrlData, Integer>, CrudRepository<UrlData, Integer>{

	UrlData findByshortUrl(String shortLink);	
}
